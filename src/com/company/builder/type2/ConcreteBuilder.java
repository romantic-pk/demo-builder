package com.company.builder.type2;

/**
 * 具体建造者，比如：麦当劳服务员
 */
public class ConcreteBuilder implements Builder{
    private Product product;

    public ConcreteBuilder() {
        product = new Product();
    }

    @Override
    public Product build() {
        return product;
    }

    @Override
    public Builder bulidA(String str) {
        product.setBuildA(str);
        return this;
    }

    @Override
    public Builder bulidB(String str) {
        product.setBuildB(str);
        return this;
    }

    @Override
    public Builder bulidC(String str) {
        product.setBuildC(str);
        return this;
    }
}
