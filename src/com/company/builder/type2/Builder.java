package com.company.builder.type2;

/**
 * 建造者，比如：麦当劳套餐建造者，声明了建造者的公共方法，用来定义建造细节方法，由子类实现具体方法
 */
public interface Builder {
    //麦辣鸡腿汉堡
    Builder bulidA(String str);
    //薯条(小)
    Builder bulidB(String str);
    //可乐
    Builder bulidC(String str);

    //获取套餐
    Product build();
}
