package com.company.builder.type2;

/**
 *  产品，比如：麦当劳套餐
 */
public class Product {

    private String buildA="麦辣鸡腿汉堡";
    private String buildB="薯条(小)";
    private String buildC="可乐";


    public String getBuildA() {
        return buildA;
    }

    public void setBuildA(String buildA) {
        this.buildA = buildA;
    }

    public String getBuildB() {
        return buildB;
    }

    public void setBuildB(String buildB) {
        this.buildB = buildB;
    }

    public String getBuildC() {
        return buildC;
    }

    public void setBuildC(String buildC) {
        this.buildC = buildC;
    }



    @Override
    public String toString() {
        return buildA+" + "+buildB+" + "+buildC+" => "+"组成一份套餐";
    }
}
