package com.company.builder.type1;

/**
 * 建造者，比如：电脑建造者，声明了建造者的公共方法，用来定义建造细节方法，由子类实现具体方法
 */
public interface ComputerBuilder {
    //构建CPU
    void buildCpu();
    //构建主板
    void buildMainboard();
    //构建内存
    void buildMemory();
    //构建硬盘
    void buildDisk();
    //构建电源
    void buildPower();

    Computer createComputer();
}
