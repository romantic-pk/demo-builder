package com.company.builder.type1;

/**
 * 产品，比如：电脑
 */
public class Computer {

    private String cpu;        //CPU
    private String memory;     //内存
    private String mainboard;  //主板
    private String disk;       //硬盘
    private String power;      //电源


    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getMemory() {
        return memory;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public String getMainboard() {
        return mainboard;
    }

    public void setMainboard(String mainboard) {
        this.mainboard = mainboard;
    }

    public String getDisk() {
        return disk;
    }

    public void setDisk(String disk) {
        this.disk = disk;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return cpu+" + "+mainboard+" + "+memory+" + "+disk+" + "+power;
    }
}
