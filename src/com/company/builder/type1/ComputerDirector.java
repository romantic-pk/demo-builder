package com.company.builder.type1;

/**
 * 指挥者，比如：电脑组装指挥者，用来指挥如何建造电脑
 */
public class ComputerDirector {

    public Computer craete(ComputerBuilder builder){
        builder.buildCpu();
        builder.buildMemory();
        builder.buildMainboard();
        builder.buildDisk();
        builder.buildPower();
        return builder.createComputer();
    }

}
