package com.company.builder.type1;

/**
 * 具体建造者，比如：联想电脑具体建造者
 */
public class LenovoComputerBuilder implements ComputerBuilder {

    private Computer computer = new Computer();

    @Override
    public void buildCpu() {
        computer.setCpu("联想公司采购的CPU");
    }

    @Override
    public void buildMainboard() {
        computer.setMainboard("联想公司采购的主板");
    }

    @Override
    public void buildMemory() {
        computer.setMemory("通用内存");
    }

    @Override
    public void buildDisk() {
        computer.setDisk("通用磁盘");
    }

    @Override
    public void buildPower() {
        computer.setPower("通用电源");
    }

    @Override
    public Computer createComputer() {
        return computer;
    }
}
