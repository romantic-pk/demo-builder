package com.company;

import com.company.builder.type1.AppleComputerBuilder;
import com.company.builder.type1.Computer;
import com.company.builder.type1.ComputerDirector;
import com.company.builder.type1.LenovoComputerBuilder;

/**
 * 测试方式1建造者模式
 */
public class TestForBuilder {

    public static void main(String[] args) {

        ComputerDirector director = new ComputerDirector();

        //构建联想品牌电脑
        Computer lenovoComputer = director.craete(new LenovoComputerBuilder());
        System.out.println("联想品牌电脑配置："+lenovoComputer.toString());

        //构建苹果品牌电脑
        Computer appleComputer = director.craete(new AppleComputerBuilder());
        System.out.println("苹果品牌电脑配置："+appleComputer.toString());
    }
}
