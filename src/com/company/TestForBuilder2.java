package com.company;

import com.company.builder.type2.ConcreteBuilder;
import com.company.builder.type2.Product;

/**
 * 测试方式2建造者模式
 */
public class TestForBuilder2 {

    public static void main(String[] args) {

        ConcreteBuilder concreteBuilder = new ConcreteBuilder();

        //默认套餐
        Product build = concreteBuilder.build();
        System.out.println("默认套餐 ："+build.toString());

        //自选套餐1
        Product build1 = concreteBuilder
                .bulidA("板烧鸡腿汉堡")
                .bulidB("薯条(中)")
                .bulidC("橙汁")
                .build();
        System.out.println("自选套餐1："+build1.toString());

        //自选套餐2
        Product build2 = concreteBuilder
                .bulidA("双层吉士汉堡")
                .bulidB("薯条(大)")
                .bulidC("冰淇淋")
                .build();
        System.out.println("自选套餐2："+build2.toString());
    }
}
